# Here go your api methods.


@auth.requires_signature()
def add_post():
    post_id = db.post.insert(
        post_title=request.vars.post_title,
        post_description=request.vars.post_description,
        post_city=request.vars.post_city,
        coordinates=request.vars.post_gps,
        post_type = request.vars.post_type,
        recovered = False,
        img_url = request.vars.img_url,
        
    )
    # We return the id of the new post, so we can insert it along all the others.
    return response.json(dict(post_id=post_id))

@auth.requires_signature()
def edit_post():
    post_id = request.vars.post_id

    db(db.post.id == post_id).update(
        post_title=request.vars.post_title,
        post_description=request.vars.post_description,
        post_city=request.vars.post_city,
        )

    return 'OK'
    
@auth.requires_signature()
def delete_post():
    post_id = request.vars.post_id
    
    db(db.post.id == post_id).delete()
    return 'OK'
    
    
def search():
    results = []
    rows = db().select(db.post.ALL, orderby=~db.post.post_time)
    p_type = request.vars.p_type
    f_city = request.vars.f_city
    
    if p_type == f_city and p_type == "":
        for row in rows:
            if row.recovered == False:
                results.append(dict(
                    id = row.id,
                    contact = "mailto:"+row.post_author,
                    post_title = row.post_title,
                    post_description = row.post_description,
                    post_city = row.post_city,
                    post_gps = row.coordinates,
                    post_type = row.post_type,
                    recovered = row.recovered,
                    img_url = row.img_url,
                ))
    elif p_type != "" and f_city == "":
        for row in rows:
            if row.post_type == p_type and row.recovered == False:
                results.append(dict(
                    id = row.id,
                    contact = "mailto:"+row.post_author,
                    post_title = row.post_title,
                    post_description = row.post_description,
                    post_city = row.post_city,
                    post_gps = row.coordinates,
                    post_type = row.post_type,
                    recovered = row.recovered,
                    img_url = row.img_url,
                ))
    elif f_city != "" and p_type == "":
        for row in rows:
            if row.post_city.lower() == f_city.lower() and row.recovered == False:
                results.append(dict(
                    id = row.id,
                    contact = "mailto:"+row.post_author,
                    post_title = row.post_title,
                    post_description = row.post_description,
                    post_city = row.post_city,
                    post_gps = row.coordinates,
                    post_type = row.post_type,
                    recovered = row.recovered,
                    img_url = row.img_url,
                ))
    else:
        for row in rows:
            if row.post_city.lower() == f_city.lower() and p_type == row.post_type and row.recovered == False:
                results.append(dict(
                    id = row.id,
                    contact = "mailto:"+row.post_author,
                    post_title = row.post_title,
                    post_description = row.post_description,
                    post_city = row.post_city,
                    post_gps = row.coordinates,
                    post_type = row.post_type,
                    recovered = row.recovered,
                    img_url = row.img_url,
                ))
    
    return response.json(dict(post_list=results))
    
    
    
    
@auth.requires_signature()
def recovered_post():
    post_id = request.vars.post_id
    db(db.post.id == post_id).update(recovered=request.vars.recovered)

    return 'ok'    
    
def get_my_post_list():
    results = []
    rows = db().select(db.post.ALL, orderby=~db.post.post_time)
    
    for row in rows:
        if str(row.post_author) == str(auth.user.email) and row.recovered == False:
            results.append(dict(
                id = row.id,
                contact = "mailto:"+row.post_author,
                post_title = row.post_title,
                post_description = row.post_description,
                post_city = row.post_city,
                post_gps = row.coordinates,
                post_type = row.post_type,
                recovered = row.recovered,
                img_url = row.img_url,
            ))
    return response.json(dict(post_list=results))

def get_my_recovered_post_list():
    results = []
    rows = db().select(db.post.ALL, orderby=~db.post.post_time)
    
    for row in rows:
        if (str(row.post_author) == str(auth.user.email)) and row.recovered == True:
            results.append(dict(
                id = row.id,
                contact = "mailto:"+row.post_author,
                post_title = row.post_title,
                post_description = row.post_description,
                post_city = row.post_city,
                post_gps = row.coordinates,
                post_type = row.post_type,
                recovered = row.recovered,
            ))
    return response.json(dict(recovered_post_list=results))

def get_recovered_post_list():
    results = []
    rows = db().select(db.post.ALL, orderby=~db.post.post_time)
    
    for row in rows:
        if row.recovered == True:
            results.append(dict(
                id = row.id,
                contact = "mailto:"+row.post_author,
                post_title = row.post_title,
                post_description = row.post_description,
                post_city = row.post_city,
                post_gps = row.coordinates,
                post_type = row.post_type,
                recovered = row.recovered,
                img_url = row.img_url,
                
            ))
    
    return response.json(dict(post_list=results))
    

    

