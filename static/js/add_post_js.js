// This is the js for the default/index.html view.
var app = function() {

    var self = {};

    Vue.config.silent = false; // show all warnings

    // Extends an array
    self.extend = function(a, b) {
        for (var i = 0; i < b.length; i++) {
            a.push(b[i]);
        }
    };
	
	
	
	//Image uploader
    self.open_uploader = function () {
        $("div#uploader_div").show();
        self.vue.is_uploading = true;
    };
	//Image uploader
    self.close_uploader = function () {
        $("div#uploader_div").hide();
        self.vue.is_uploading = false;
        $("input#file_input").val(""); // This clears the file choice once uploaded.

    };
	
    self.upload_file = function (event) {
        // This function is in charge of: 
        // - Creating an image preview
        // - Uploading the image to GCS
        // - Calling another function to notify the server of the final image URL.

        // Reads the file.
        var input = event.target;
        var file = input.files[0];
        if (file) {
            // We want to read the image file, and transform it into a data URL.
            var reader = new FileReader();
            // We add a listener for the load event of the file reader.
            // The listener is called when loading terminates.
            // Once loading (the reader.readAsDataURL) terminates, we have
            // the data URL available. 
            reader.addEventListener("load", function () {
                // An image can be represented as a data URL.
                // See https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Data_URIs
                // Here, we set the data URL of the image contained in the file to an image in the
                // HTML, causing the display of the file image.
                self.vue.img_url = reader.result;
            }, false);
            // Reads the file as a data URL. This triggers above event handler. 
            reader.readAsDataURL(file);

            // Now we should take care of the upload.
            // Gets an upload URL.
            console.log("Trying to get the upload url");
            $.getJSON('https://upload-dot-luca-teaching.appspot.com/start/uploader/get_upload_url',
                function (data) {
                    // We now have upload (and download) URLs.
                    // The PUT url is used to upload the image.
                    // The GET url is used to notify the server where the image has been uploaded;
                    // that is, the GET url is the location where the image will be accessible 
                    // after the upload.  We pass the GET url to the upload_complete function (below)
                    // to notify the server. 
                    var put_url = data['signed_url'];
                    var get_url = data['access_url'];
                    console.log("Received upload url: " + put_url);
                    // Uploads the file, using the low-level interface.
                    var req = new XMLHttpRequest();
                    // We listen to the load event = the file is uploaded, and we call upload_complete.
                    // That function will notify the server of the location of the image. 
                    req.addEventListener("load", self.upload_complete(get_url));
                    // TODO: if you like, add a listener for "error" to detect failure.
                    req.open("PUT", put_url, true);
                    req.send(file);
                });
        }
    };
	
    self.upload_complete = function(get_url) {
        // Hides the uploader div.
        self.vue.show_img = true;
        self.close_uploader();
        console.log('The file was uploaded; it is now available at ' + get_url);
		self.vue.image_url = get_url;
        // TODO: The file is uploaded.  Now you have to insert the get_url into the database, etc.
    };
	
    self.add_post = function () {
        // We disable the button, to prevent double submission.
        $.web2py.disableElement($("#add-post"));
        var sent_title = self.vue.form_title; // Makes a copy 
        var sent_description = self.vue.form_description; // 
		var sent_city = self.vue.form_city;
		var sent_gps = self.vue.form_gps;
		var found_lost = self.vue.picked;
		var post_image_url = self.vue.image_url;
		console.log(post_image_url);
		
        $.post(add_post_url,
            // Data we are sending.
            {
                post_title: self.vue.form_title,
                post_description: self.vue.form_description,
				post_city: self.vue.form_city,
				post_gps: self.vue.form_gps,
				post_type: self.vue.picked,
				img_url:self.vue.image_url,
            },
            // What do we do when the post succeeds?
            function (data) {
                // Re-enable the button.
                $.web2py.enableElement($("#add-post"));
                // Clears the form.
                self.vue.form_title = "";
                self.vue.form_description = "";
				window.location.href = '/nalowale/default/my_posts';
                // Adds the post to the list of posts. 
                // We re-enumerate the array.
            });
        // If you put code here, it is run BEFORE the call comes back.
    };


	self.vue = new Vue({
    	el: "#vue-div",
    	delimiters: ['${', '}'],
    	unsafeDelimiters: ['!{', '}'],
    	data: {
			picked:'',
    		form_title: '',
        	form_description: '',
			form_city: '',
			form_gps: '',
        	post_list: [],
			title_maxCount: 50,
        	title_remainingCount: 50,
			description_maxCount: 300,
			description_remainingCount:300,
			location_remainingCount:40,
			city_maxCount: 30,
			city_remainingCount: 30,
			is_uploading: false,
			image_url:null,
			show_img:false
			
   	 },
    	methods: {
        	add_post: self.add_post,
			title_countdown: function() {
				this.title_remainingCount = this.title_maxCount - this.form_title.length;
			      //this.hasError = this.title_remainingCount < 0;
			},
			description_countdown: function() {
				this.description_remainingCount = this.description_maxCount - this.form_description.length;
				      //this.hasError = this.title_remainingCount < 0;
			},
			city_countdown: function() {
				this.city_remainingCount = this.city_maxCount - this.form_city.length;
					      //this.hasError = this.title_remainingCount < 0;
			},
			open_uploader:self.open_uploader,
			close_uploader:self.close_uploader,
			upload_file:self.upload_file,
			upload_complete:self.upload_complete
		
    	}

	});
	
};




var APP = null;
jQuery(function(){APP = app();});